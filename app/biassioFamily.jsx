import React from 'react'
import Member from './member'

export default props => (
    <div>
        <Member name="Rafael" lastName="Biassio" />
        <Member name="Giovani" lastName="Biassio" />
        <Member name="Elza" lastName="Biassio" />
    </div>
)
