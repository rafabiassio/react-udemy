import React from 'react'

const Primeiro = props => <h1>First!</h1>

const Segundo = props => <h1>Second!</h1>

export { Primeiro, Segundo }