 import React from 'react'

 export default props => (
     <div>
         { React.Children.map(props.children, 
            item => React.cloneElement(item, {...props})) }
     </div>
 )